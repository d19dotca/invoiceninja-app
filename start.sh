#!/bin/bash
set -eu

readonly ARTISAN="php /app/code/artisan"
readonly COMPOSER="sudo -u www-data composer --working-dir=/app/code"

echo "==> Startup"

# ensure directories
mkdir -p /run/invoiceninja/sessions /run/invoiceninja/bootstrap-cache /run/invoiceninja/cache /run/invoiceninja/logs /run/cron

echo "==> Create php.ini"
cp /app/pkg/php.ini /run/php.ini
memory_full=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes) # this is the RAM. we have equal amount of swap
memory_limit=$((memory_full/1024/1024)) # we will give php 50% of the whole memory, so just the allocated RAM
crudini --set /run/php.ini PHP memory_limit ${memory_limit}M

# Settings which should be updated only once
if [[ ! -f "/app/data/env" ]]; then
    echo "==> Creating initial /app/data/env file"
    sed -e "s|.*\(API_SECRET\).*|\1=$(pwgen -1cns 32)|g" \
        -e "s|.*\(MAIL_FROM_NAME\).*|\1=InvoiceNinja|g" \
        -e "s|.*\(PRECONFIGURED_INSTALL\).*|\1=true|g" \
        /app/pkg/env.template > /app/data/env # sed -i seems to destroy symlink

    # generate the APP_KEY. This is run as root because of permissions
    $ARTISAN key:generate --force --no-interaction
fi

# Settings to be updated on every run.
echo "==> Update env file for database and email configs"
sed -e "s|.*\(APP_URL\).*|\1=${CLOUDRON_APP_ORIGIN}|g" \
    -e "s|.*\(DB_TYPE\).*|\1=mysql|g" \
    -e "s|.*\(DB_HOST\).*|\1=${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}|g" \
    -e "s|.*\(DB_DATABASE\).*|\1=${CLOUDRON_MYSQL_DATABASE}|g" \
    -e "s|.*\(DB_USERNAME\).*|\1=${CLOUDRON_MYSQL_USERNAME}|g" \
    -e "s|.*\(DB_PASSWORD\).*|\1=${CLOUDRON_MYSQL_PASSWORD}|g" \
    -e "s|.*\(MAIL_DRIVER\).*|\1=smtp|g" \
    -e "s|.*\(MAIL_PORT\).*|\1=${CLOUDRON_MAIL_SMTP_PORT}|g" \
    -e "s|.*\(MAIL_ENCRYPTION\).*|\1=|g" \
    -e "s|.*\(MAIL_HOST\).*|\1=${CLOUDRON_MAIL_SMTP_SERVER}|g" \
    -e "s|.*\(MAIL_USERNAME\).*|\1=${CLOUDRON_MAIL_SMTP_USERNAME}|g" \
    -e "s|.*\(MAIL_FROM_ADDRESS\).*|\1=${CLOUDRON_MAIL_FROM}|g" \
    -e "s|.*\(MAIL_PASSWORD\).*|\1=${CLOUDRON_MAIL_SMTP_PASSWORD}|g" \
    -e "s|.*\(REQUIRE_HTTPS\).*|\1=true|g" \
    -i /app/data/env

if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "==> Copying files on first run"
    cp -r /app/code/storage-vanilla /app/data/storage

    mkdir -p /app/data/public
    cp -r /app/code/public-logo-vanilla /app/data/public/logo

    # $COMPOSER dump-autoload --optimize --no-interaction
    # $ARTISAN optimize --force --no-interaction --verbose
    $ARTISAN migrate --force --no-interaction --verbose
    $ARTISAN db:seed --force --no-interaction --verbose

    touch "/app/data/.dbsetup"
else
    echo "==> Run db migration"
    # Put the application into maintenance mode
    $ARTISAN down --no-interaction --verbose

    # Run the database migrations
    $ARTISAN migrate --force --no-interaction --verbose

    # Optimize the framework for better performance
    # $ARTISAN optimize --force --no-interaction --verbose

    # Bring the application out of maintenance mode
    $ARTISAN up --no-interaction --verbose
fi

# sessions directory
rm -rf /app/data/storage/framework/sessions && ln -s /run/invoiceninja/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/invoiceninja/cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/invoiceninja/logs /app/data/storage/logs

# clear cached stuff under /app/data/storage/framework (https://github.com/laravel/framework/issues/17377)
$ARTISAN view:clear
$ARTISAN cache:clear

[[ ! -f /app/data/crontab ]] && cp /app/pkg/crontab.template /app/data/crontab

# configure in-container Crontab - http://www.gsp.com/cgi-bin/man.cgi?section=5&topic=crontab
# we run as root user so that the cron tasks can redirect properly to the cron's stdout/stderr
if ! (env; cat /app/data/crontab; echo -e '\nMAILTO=""') | crontab -u root -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

# ensure permissions are set correctly
chown -R www-data:www-data /app/data /run/invoiceninja

echo "==> Starting InvoiceNinja"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i InvoiceNinja

